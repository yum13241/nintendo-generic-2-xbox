#!/usr/bin/env python

# https://gist.github.com/dnmodder/de2df973323b7c6acf45f40dc66e8db3

import usb.core
import systemd.journal

def jprint(*args):
	print(*args)
	systemd.journal.send(*args, SYSLOG_IDENTIFIER='nintendo-generic-2-xbox')

dev = usb.core.find(idVendor=0x045e, idProduct=0x028e)

if dev is None:
	jprint('Generic Nintendo controller not found.')
else:
	dev.ctrl_transfer(0xc1, 0x01, 0x0100, 0x00, 0x14) 
	jprint('Generic Nintendo controller found and forced to XBox mode.')
